<GameFile>
  <PropertyGroup Name="MainScene" Type="Scene" ID="a2ee0952-26b5-49ae-8bf9-4f1d6279b798" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="65" Speed="1.0000">
        <Timeline ActionTag="953446860" Property="FileData">
          <TextureFrame FrameIndex="0" Tween="False">
            <TextureFile Type="Normal" Path="bg.png" Plist="" />
          </TextureFrame>
        </Timeline>
        <Timeline ActionTag="953446860" Property="BlendFunc">
          <BlendFuncFrame FrameIndex="0" Tween="False" Src="1" Dst="771" />
        </Timeline>
        <Timeline ActionTag="838736683" Property="Position">
          <PointFrame FrameIndex="0" X="254.9941" Y="88.6000">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="838736683" Property="RotationSkew">
          <ScaleFrame FrameIndex="0" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="10" X="3.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="30" X="-3.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="40" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="838736683" Property="FileData">
          <TextureFrame FrameIndex="0" Tween="False">
            <TextureFile Type="Normal" Path="cat_normal.png" Plist="" />
          </TextureFrame>
          <TextureFrame FrameIndex="50" Tween="False">
            <TextureFile Type="Normal" Path="cat_leftfoot.png" Plist="" />
          </TextureFrame>
          <TextureFrame FrameIndex="55" Tween="False">
            <TextureFile Type="Normal" Path="cat_normal.png" Plist="" />
          </TextureFrame>
          <TextureFrame FrameIndex="60" Tween="False">
            <TextureFile Type="Normal" Path="cat_rightfoot.png" Plist="" />
          </TextureFrame>
          <TextureFrame FrameIndex="65" Tween="False">
            <TextureFile Type="Normal" Path="cat_normal.png" Plist="" />
          </TextureFrame>
        </Timeline>
        <Timeline ActionTag="838736683" Property="BlendFunc">
          <BlendFuncFrame FrameIndex="0" Tween="False" Src="1" Dst="771" />
        </Timeline>
        <Timeline ActionTag="838736683" Property="AnchorPoint">
          <ScaleFrame FrameIndex="0" X="0.5000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
      </Animation>
      <AnimationList>
        <AnimationInfo Name="Idle" StartIndex="0" EndIndex="40">
          <RenderColor A="150" R="255" G="255" B="0" />
        </AnimationInfo>
        <AnimationInfo Name="Walk" StartIndex="45" EndIndex="65">
          <RenderColor A="150" R="250" G="235" B="215" />
        </AnimationInfo>
      </AnimationList>
      <ObjectData Name="Scene" ctype="GameNodeObjectData">
        <Size X="960.0000" Y="640.0000" />
        <Children>
          <AbstractNodeData Name="Background" ActionTag="953446860" Tag="5" IconVisible="False" ctype="SpriteObjectData">
            <Size X="960.0000" Y="640.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="480.0000" Y="320.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="bg.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Cat" ActionTag="838736683" Tag="7" RotationSkewX="0.3000" IconVisible="False" LeftMargin="181.9941" RightMargin="632.0059" TopMargin="305.4000" BottomMargin="88.6000" ctype="SpriteObjectData">
            <Size X="146.0000" Y="246.0000" />
            <AnchorPoint ScaleX="0.5000" />
            <Position X="254.9941" Y="88.6000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2656" Y="0.1384" />
            <PreSize X="0.1521" Y="0.3844" />
            <FileData Type="Normal" Path="cat_normal.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>