
var HelloWorldLayer = cc.Layer.extend({
    ctor:function () {
        //////////////////////////////
        // 1. super init first
        this._super();

        var mainscene = ccs.load(res.MainScene_json);
        this.addChild(mainscene.node);

        this.runAction(mainscene.action);
        mainscene.action.play("Walk");

        var cat = mainscene.node.getChildByName("Cat");
        var move = new cc.MoveBy(10,cc.p(480,0));
        cat.runAction(move);

        return true;
    }
});

var HelloWorldScene = cc.Scene.extend({
    onEnter:function () {
        this._super();
        var layer = new HelloWorldLayer();
        this.addChild(layer);
    }
});

